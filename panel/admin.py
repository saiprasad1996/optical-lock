from django.contrib import admin
from panel.models import User

# Register your models here.

admin.site.register(User)
admin.site.site_header = "Optical Lock Admin"
admin.site.site_title = "Optical Lock Portal"
admin.site.index_title = "Optical Lock Portal"