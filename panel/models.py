from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from panel.exceptions import LoginFailed


class User(models.Model):
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    @staticmethod
    def login(username, password):
        try:
            user = User.objects.get(username=username, password=password)
            return user
        except ObjectDoesNotExist:
            raise LoginFailed

    def __str__(self):
        return self.username
