from django.shortcuts import render, redirect

# Create your views here.
from django.views import View

from panel.exceptions import LoginFailed, NoSession
from panel.forms import *
from panel.models import *


class Login(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'panel/login.html')

    def post(self, request, *args, **kwargs):
        try:
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = User.login(username, password)
            request.session['user'] = username
            return redirect('panel_home')
        except LoginFailed:
            return render(request, 'panel/login.html',
                          {'error_message': 'Login Failed. Please check your username and password'})



def checkSession(request):
    user = request.session.get('user')
    if user is not None:
        user = User.objects.get(username=user)
        return user
    else:
        raise NoSession
