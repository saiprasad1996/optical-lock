class LoginFailed(BaseException):

    def __str__(self):
        return 'Login Failed'


class NoSession(BaseException):

    def __str__(self):
        return 'No Session'
